#!/bin/bash -x

source /host/settings.sh

get_public_ip() {
    local services='ifconfig.co ifconfig.me icanhazip.com'
    local pattern='^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$'
    local ip
    for service in $services; do
        ip=$(curl -s $service)
        [[ $ip =~ $pattern ]] && echo $ip && return
    done
    return 1
}

### fix config files
sed -i /etc/bind/zone/example.org.db \
    -e "s/127\.0\.0\.1/$(get_public_ip)/"

### fix default options
sed -i /etc/default/named \
    -e '/^OPTIONS=/ c OPTIONS="-4 -u bind"'

### fix permissions
chown bind: -R /etc/bind

### add ufw rules for allowing axfr servers
while read line; do
    line=$(echo $line | cut -d'#' -f1)
    [[ -z $line ]] && continue
    server_ip=$(echo $line | cut -d';' -f1)
    ufw allow from $server_ip to any port 53
done <<< "$AXFR_SERVERS"
ufw enable

cat <<_EOF > /etc/bind/conf/allow-transfer
allow-transfer {
    127.0.0.1;  # for debugging $AXFR_SERVERS};
_EOF

cat <<_EOF > /etc/bind/conf/also-notify
also-notify {$AXFR_SERVERS};
_EOF

> /etc/bind/conf/secondary.ns
for ns in $SECONDARY_NS; do
    echo "@  IN  NS  ${ns}." >> /etc/bind/conf/secondary.ns
done
